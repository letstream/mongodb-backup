FROM ubuntu:20.04

RUN apt-get update && apt-get install -y wget

RUN wget https://fastdl.mongodb.org/tools/db/mongodb-database-tools-ubuntu2004-x86_64-100.6.1.deb 
RUN apt-get install -y ./mongodb-database-tools-*-100.6.1.deb

RUN wget https://dl.min.io/client/mc/release/linux-amd64/mc && mv mc /usr/bin && chmod +x /usr/bin/mc
